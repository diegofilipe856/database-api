import sqlite3
from uuid import uuid4
from data_manipulation import *
connection = sqlite3.connect("./database.db")
client = connection.cursor()


def rescue_all_students():
    students = client.execute('SELECT * FROM alunos')
    students = client.fetchall()
    name_iterator = 1
    cpf_iterator = 2
    age_iterator = 3
    graduation_id_iterator = 7
    for student in students:
        name = student[name_iterator]
        age = student[age_iterator]
        cpf = student[cpf_iterator]
        graduation = client.execute(f'SELECT nome FROM cursos WHERE id = "{student[graduation_id_iterator]}"')
        graduation = client.fetchall()
        graduation = graduation[0][0]
        print(f'''
Nome: {name}
Idade: {age}
CPF: {cpf}
Curso: {graduation}''')
        print()

def rescue_student():
    student = input("Digite o CPF do estudante: ")
    student_id = get_student_id(student)
    graduation = client.execute(f'SELECT curso_id FROM alunos WHERE cpf = "{student}"')
    graduation = client.fetchall()
    graduation = graduation[0][0]
    graduation = client.execute(f'SELECT nome FROM cursos WHERE id = "{graduation}"')
    graduation = client.fetchall()
    graduation = graduation[0][0]
    subjects_ids = client.execute(f'SELECT disciplina_id FROM aluno_disciplina WHERE aluno_id = "{student_id}"')
    subjects_ids = client.fetchall()
    array_of_subjects = []
    for subject in subjects_ids:
        subject = subject[0]
        temp_dict = {}
        name = client.execute(f'SELECT nome FROM disciplinas WHERE id = "{subject}"')
        name = client.fetchall()
        code = client.execute(f'SELECT codigo FROM disciplinas WHERE id = "{subject}"')
        code = client.fetchall()
        temp_dict["name"] = name
        temp_dict["code"] = code
        array_of_subjects.append(temp_dict)
    student_name = client.execute(f'SELECT nome FROM alunos WHERE id = "{student_id}"')
    student_name = client.fetchall()
    student_name = student_name[0][0]
    print(f'Nome do aluno: {student_name}')
    print(f'Curso: {graduation}')
    print('O aluno está matriculado nas seguintes disciplinas: ')
    for iterator in array_of_subjects:
        print(f"Disciplina: {iterator['name'][0][0]}; Código: {iterator['code'][0][0]}")


def students_in_graduation():
    graduation = input("Digite o nome do curso: ")
    graduation_id = get_graduation_id(graduation)
    students_in = client.execute(f'SELECT * from alunos WHERE curso_id = "{graduation_id}"')
    students_in = client.fetchall()
    students_in = students_in
    name_iterator = 1
    cpf_iterator = 2
    print(f"Esses são os alunos matriculados em {graduation}: ")
    for iterator in students_in:
        print(f"Nome: {iterator[name_iterator]}")
        print(f"CPF: {iterator[cpf_iterator]}")
        print()


def students_in_subject():
    subject = input("Digite o código da disciplina: ")
    subject_id = get_subject_id(subject)
    students_joined = client.execute(f'SELECT * FROM aluno_disciplina WHERE disciplina_id = "{subject_id}"')
    students_joined = client.fetchall()
    name_iterator = 1
    print(f"Esses são os alunos matriculados a disciplina {subject}:")
    for iterator in students_joined:
        student = client.execute(f'SELECT nome FROM alunos WHERE id = "{iterator[name_iterator]}"')
        student = client.fetchall()
        print(student[0][0])

def ranking_of_subjects():
    client.execute('SELECT * FROM disciplinas')
    subjects = client.fetchall()
    id_iterator = 0
    code_iterator = 1
    array_of_subjects = []
    for subject in subjects:
        client.execute(f'SELECT codigo FROM disciplinas WHERE id = "{subject[id_iterator]}"')
        code = client.fetchall()
        code = code[0][0]
        temp_dict = {}
        temp_dict["code"] = code
        client.execute(f'SELECT * FROM aluno_disciplina WHERE disciplina_id = "{subject[id_iterator]}"')
        number_of_students = client.fetchall()
        number_of_students = len(number_of_students)
        temp_dict["number_of_students"] = number_of_students
        array_of_subjects.append(temp_dict)
    array_of_subjects = sorted(array_of_subjects, key=lambda x: x['number_of_students'], reverse=True)
    print("Esse é o ranking de disciplinas com mais alunos: ")
    for subject in array_of_subjects:
        print(f'Código: {subject["code"]}')
        print(f'Número de estudantes: {subject["number_of_students"]}')
        print()