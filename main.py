from registration import *
from read import *
operation = 'S'
while operation != 'N':
    option = '0'
    while option not in '12':
        option = input('''Selecione uma ação: 
    [1] Modo cadastro
    [2] Modo leitura
    Digite: ''')
    if option == '1':
        choice = '0'
        while choice not in '12345678':
            choice = input('''O que você quer cadastrar?
    [1] Novo aluno
    [2] Novo curso
    [3] Novo professor
    [4] Nova disciplina
    [5] Alterar curso de um aluno
    [6] Matricular um aluno numa disciplina
    [7] Desmatricular um aluno de uma disciplina
    [8] Alterar o professor responsável por uma disciplina
    Selecione uma opção: ''')
        if choice == '1':
            new_student()
        elif choice == '2':
            new_graduation()
        elif choice == '3':
            new_teacher()
        elif choice == '4':
            new_subject()
        elif choice == '5':
            modify_students_graduation()
        elif choice == '6':
            enroll_student()
        elif choice == '7':
            unenroll_student()
        elif choice == '8':
            change_teacher_responsible()


    elif option == '2':
        choice = '0'
        while choice not in '12345':
            choice = input('''O que você quer pesquisar?
    [1] Resgatar todos os alunos
    [2] Curso e disciplinas de um aluno
    [3] Lista de alunos associados a um curso
    [4] Lista de alunos matriculados em uma disciplina
    [5] Ranking de disciplinas
    Digite: ''')
        if choice == '1':
            rescue_all_students()
        elif choice == '2':
            rescue_student()
        elif choice == '3':
            students_in_graduation()
        elif choice == '4':
            students_in_subject()
        elif choice == '5':
            ranking_of_subjects()

    operation = input("Deseja fazer outra operação [S/N]: ").upper()
print("Tchau! :D")

