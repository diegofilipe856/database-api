import sqlite3
from uuid import uuid4
from data_manipulation import *
connection = sqlite3.connect("./database.db")
client = connection.cursor()


def new_student():
    student = input("Digite o nome do aluno: ")
    cpf = input("Digite o cpf do aluno: ")
    age = int(input("Digite a idade do aluno: "))
    rg = input("Digite o RG do aluno: ")
    dispatching_agency = input("Digite o orgão expeditor: ")
    birthday = input("Digite a data de aniversário: ")
    student_id = str(uuid4())
    student_graduation_id = None
    while student_graduation_id == None:
        student_graduation = input("Digite a graduação do aluno: ")
        student_graduation_id = get_graduation_id(student_graduation)
    db_rg = f'"{rg}"' if rg is not None else "NULL"
    db_dispatching_agency = f'"{dispatching_agency}"' if dispatching_agency is not None else "NULL"
    client.execute(f'INSERT INTO alunos values("{student_id}", "{student}", "{cpf}", {age}, {db_rg}, {db_dispatching_agency}, "{birthday}", "{student_graduation_id}");')
    connection.commit()
    print("Cadastro realizado com sucesso!")
def new_graduation():
    graduation_name = input("Digite o nome do curso: ")
    year_of_creation = int(input("Digite o ano de criação do curso: "))
    coordinator = input("Digite o CPF do coordenador do curso: ")
    verify_coordinator = client.execute(f'SELECT * FROM professores  WHERE cpf = "{coordinator}"')
    verify_coordinator = client.fetchall()
    if verify_coordinator != []:
        building_name = input("Digite o prédio onde o curso será ministrado: ")
        graduation_id = str(uuid4())
        client.execute(f'INSERT INTO cursos values("{graduation_id}", "{graduation_name}", {year_of_creation}, "{verify_coordinator[0][0]}", "{building_name}");')
        connection.commit()
        print("Operação realizada com sucesso!")
    else:
        print("O coordenador não é um professor registrado. Tente novamente.")


def new_teacher():
    name_of_teacher = input("Digite o nome do professor: ")
    cpf_teacher = input("Digite o CPF do professor: ")
    if cpf_teacher:
        titulation = input("Digite a titulação: ")
        teacher_id = str(uuid4())

        client.execute(f'INSERT INTO professores values("{teacher_id}", "{name_of_teacher}", "{cpf_teacher}", "{titulation}");')
        connection.commit()
        print("Operação realizada com sucesso!")
    else:
        print("CPF obrigatório.")


def new_subject():
    subject_code = input("Digite o código da disciplina: ")
    subject_name = input("Digite o nome da disciplina: ")
    teacher_cpf = input("Digite o CPF do professor responsável pela disciplina: ")
    teacher_id = get_teacher_id(teacher_cpf)
    verify_teacher = client.execute(f'SELECT * FROM professores WHERE cpf = "{teacher_cpf}"')
    verify_teacher = client.fetchall()
    if verify_teacher != []:

        description = input("Dê uma breve descrição da disciplina: ")
        subject_id = str(uuid4())

        client.execute(f'INSERT INTO disciplinas values("{subject_id}", "{subject_code}", "{subject_name}", "{teacher_id}", "{description}");')
        connection.commit()
        print("Operação realizada com sucesso!")


def modify_students_graduation():
    chosen_student = input("Digite o CPF do estudante: ")
    updated_graduation = input("Digite o nome da nova graduação: ")
    updated_graduation_id = client.execute(f'SELECT id FROM cursos WHERE nome = "{updated_graduation}"')
    updated_graduation_id = client.fetchall()
    if updated_graduation_id != []:
        updated_graduation_id = updated_graduation_id[0][0]
        client.execute(f'UPDATE alunos SET curso_id = "{updated_graduation_id}" WHERE cpf = "{chosen_student}";')
        connection.commit()
        print("Operação realizada com sucesso!")
    else:
        print("Graduação não cadastrada.")


def enroll_student():
    student_cpf = input("Digite o CPF do estudante a ser matriculado: ")
    student_id = get_student_id(student_cpf)
    if student_id != None:
        subject = input("Digite o código da disciplina: ")
        subject_id = get_subject_id(subject)
        if subject_id != None:
            client.execute(f'INSERT INTO aluno_disciplina VALUES("{subject_id}", "{student_id}");')
            connection.commit()
            print("Operação realizada com sucesso!")


def unenroll_student():
    student_cpf = input("Digite o CPF do estudante a ser desmatriculado: ")
    student_id = get_student_id(student_cpf)
    if student_id != None:
        subject = input("Digite o código da disciplina: ")
        subject_id = get_subject_id(subject)
        if subject_id != None:
            client.execute(f'DELETE FROM aluno_disciplina WHERE disciplina_id = "{subject_id}" AND aluno_id = "{student_id}";')
            connection.commit()
            print("Operação realizada com sucesso!")


def change_teacher_responsible():
    subject = input("Digite o código da disciplina: ")
    subject_id = get_subject_id(subject)
    if subject_id != None:
        teacher_to_replace = input("Digite o CPF do novo professor: ")
        teacher_id = get_teacher_id(teacher_to_replace)
        if teacher_id != None:
            client.execute(f'UPDATE disciplinas SET professor = "{teacher_id}" WHERE id = "{subject_id}"')
            print("Operação realizada com sucesso!")
            connection.commit()

